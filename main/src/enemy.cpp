#include <iostream>
#include <string>
#include <cmath>
#include "enemy.hpp"
#include "streams.hpp"

Enemy::Enemy()
{
	std::cout << "Creation of enemy." << std::endl;
}

void Enemy::Write(OutputStream& stream)
{
	/* Position */
	uint32_t c_xPos = static_cast<uint32_t> ((m_xPos + 500) * 1000);
	uint32_t c_yPos = static_cast<uint32_t> ((m_yPos + 500) * 1000);
	uint32_t c_zPos = static_cast<uint32_t> ((m_zPos + 500) * 1000);

	/* Rotation */
	float rotArray[4] = { m_xRot, m_yRot, m_zRot, m_tRot };
	std::vector<uint16_t> compressedRotArray;
	std::transform(rotArray, rotArray + 4, std::back_inserter(compressedRotArray), [](float rot) -> uint16_t
		{return static_cast<uint16_t> ((rot + 0.707) * 1000); });
	auto max = std::distance(rotArray, std::max_element(rotArray, rotArray + 4));
	uint32_t compressedQuaternion = 0;
	int k = 0;
	for (size_t i = 0; i < 4; i++)
	{
		if (i != max)
		{
			compressedQuaternion |= (compressedRotArray[i] << (22 - (10 * k)));
			k++;
		}
	}
	compressedQuaternion |= max;

	stream.WriteStr(m_type);
	stream.Write(c_xPos);
	stream.Write(c_yPos);
	stream.Write(c_zPos);
	stream.Write(compressedQuaternion);
}

void Enemy::Read(InputStream& stream)
{
	if (m_firstRead)
	{
		m_firstRead = false;
	}
	else
	{
		std::cout << "Update of enemy." << std::endl;
	}

	m_type = stream.ReadStr();
	uint32_t c_xPos = stream.Read<uint32_t>();
	uint32_t c_yPos = stream.Read<uint32_t>();
	uint32_t c_zPos = stream.Read<uint32_t>();
	uint32_t c_Quaternion = stream.Read<uint32_t>();

	m_xPos = (c_xPos / 1000.f) - 500.f;
	m_yPos = (c_yPos / 1000.f) - 500.f;
	m_zPos = (c_zPos / 1000.f) - 500.f;

	uint32_t max = c_Quaternion & 3;
	c_Quaternion >>= 2;
	float quat1 = (c_Quaternion & 0x3ff) / 1000.f - 0.707f;
	c_Quaternion >>= 10;
	float quat2 = (c_Quaternion & 0x3ff) / 1000.f - 0.707f;
	c_Quaternion >>= 10;
	float quat3 = (c_Quaternion & 0x3ff) / 1000.f - 0.707f;

	float quat4 = sqrt(1 - (quat1 * quat1) - (quat2 * quat2) - (quat3 * quat3));

	switch (max)
	{
	case 0:
		m_xRot = quat4;
		m_yRot = quat3;
		m_zRot = quat2;
		m_tRot = quat1;
		break;
	case 1:
		m_xRot = quat3;
		m_yRot = quat4;
		m_zRot = quat2;
		m_tRot = quat1;
		break;
	case 2:
		m_xRot = quat3;
		m_yRot = quat2;
		m_zRot = quat4;
		m_tRot = quat1;
		break;
	case 3:
		m_xRot = quat3;
		m_yRot = quat2;
		m_zRot = quat1;
		m_tRot = quat4;
		break;
	}
}

void Enemy::Destroy()
{
	std::cout << "Destruction of enemy" << std::endl;
}

void Enemy::SetPos(float x, float y, float z)
{
	m_xPos = x;
	m_yPos = y;
	m_zPos = z;
}

std::string Enemy::ToString() const
{
	std::string res;
	res += "Ennemy Type: " + m_type + "\n";
	res += "Position: " + std::to_string(m_xPos) + "/" + std::to_string(m_yPos) + "/" + std::to_string(m_zPos) + "\n";
	res += "Rotation: " + std::to_string(m_xRot) + "/" + std::to_string(m_yRot) + "/" + std::to_string(m_zRot) + "/"
		+ std::to_string(m_tRot);
	return res;
}
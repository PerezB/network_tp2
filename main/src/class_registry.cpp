#include "class_registry.hpp"

#include "enemy.hpp"
#include "player.hpp"

ClassRegistry::ClassRegistry()
{
	RegisterClass<Enemy>();
	RegisterClass<Player>();
}

GameObject* ClassRegistry::Create(ReplicationClassID id) const
{
	auto creationFunction = m_classRegistry.at(id);
	return creationFunction();
}
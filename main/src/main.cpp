#include <iostream>
#include <string>

#include "server.hpp"
#include "client.hpp"
#include "replication_manager.hpp"
#include "game_object.hpp"
#include "player.hpp"
#include "enemy.hpp"
#include "streams.hpp"

int main(int argc, char* argv[]) 
{
	if (argc == 4)
	{
		std::string clientOrServer(argv[1]);
		std::string ip(argv[2]);
		std::string port(argv[3]);

		ReplicationManager replicationManager;


		if (clientOrServer == "server")
		{
			Server server(ip, stoi(port));

			std::cout << "Press a key to start replication" << std::endl;
			std::cin.get();
			
			std::vector<GameObject*> gameObjects;
			auto go1 = std::make_shared<Player>();
			gameObjects.push_back(go1.get());

			auto go2 = std::make_shared<Enemy>();
			gameObjects.push_back(go2.get());

			OutputStream out;
			replicationManager.Replicate(out, gameObjects);
			auto streamData = out.Data();
			server.Send(reinterpret_cast<uint8_t*>(streamData.data()), streamData.size());

			std::cout << "Press a key to start replication" << std::endl;
			std::cin.get();

			Player* player = reinterpret_cast<Player*>(gameObjects.at(0));
			player->SetPos(1, 1, 1);
			gameObjects.pop_back();

			out.Flush();
			replicationManager.Replicate(out, gameObjects);
			streamData = out.Data();
			server.Send(reinterpret_cast<uint8_t*>(streamData.data()), streamData.size());

			std::cout << "Press a key to exit" << std::endl;
			std::cin.get();
		}
		else if (clientOrServer == "client")
		{
			std::cout << "Press a key to exit" << std::endl;
			std::cin.get();

			Client client(ip, stoi(port));

			std::cout << "Press a key to exit" << std::endl;
			std::cin.get();
		}
		else
		{
			std::cout << "Error: first argument must be \"client\" or \"server\"." << std::endl;
			return EXIT_FAILURE;
		}

		return EXIT_SUCCESS;
	}
	else
	{
		std::cout << "Error: you must give 3 arguments: " << argc << " given." << std::endl;
		return EXIT_FAILURE;
	}
}
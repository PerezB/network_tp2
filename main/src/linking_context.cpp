#include "linking_context.hpp"

#include <iostream>
#include <cassert>

LinkingContext::LinkingContext() {};

uint32_t LinkingContext::AddGameObject(GameObject* object)
{
	AddGameObject(object, m_nextId);
	return (m_nextId++);
}

void LinkingContext::AddGameObject(GameObject* object, uint32_t networkId)
{
	// In case we add an object already inside the linking context. 
	// The value in m_gameObjectToId will be override but we have 
	// to remove the old id from m_idToGameObject
	std::optional<uint32_t> existingNetworkId = GetNetworkId(object);
	if (existingNetworkId.has_value())
	{
		m_idToGameObject.erase(existingNetworkId.value());
	}

	m_gameObjectToId[object] = networkId;
	m_idToGameObject[networkId] = object;
}

void LinkingContext::RemoveGameObject(GameObject* object)
{
	std::optional<uint32_t> networkId = GetNetworkId(object);
	assert(networkId.has_value());

	m_gameObjectToId.erase(object);
	m_idToGameObject.erase(networkId.value());
}

std::optional<uint32_t> LinkingContext::GetNetworkId(GameObject* object)
{
	auto networkIdIterator = m_gameObjectToId.find(object);
	if (networkIdIterator != m_gameObjectToId.end())
	{
		return networkIdIterator->second;
	}
	else
	{
		return {};
	}
}

std::optional<GameObject*> LinkingContext::GetGameObject(uint32_t networkId)
{
	auto gameObjectIterator = m_idToGameObject.find(networkId);
	if (gameObjectIterator != m_idToGameObject.end())
	{
		return gameObjectIterator->second;
	}
	else
	{
		return {};
	}
}

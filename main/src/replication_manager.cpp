#include "replication_manager.hpp"
#include "game_object.hpp"

#include "class_registry.hpp"

#include <iostream>

void ReplicationManager::Replicate(OutputStream& stream, const std::vector<GameObject*>& gameObjects)
{
	stream.Write(PROTOCOL_ID);
	// Can't to pass value directly to the stream for some reason
	PacketID temp = PacketID::Replication;
	stream.Write(temp);

	for (GameObject* gameObject : gameObjects)
	{
		std::optional<uint32_t> networkId = m_linkingContext.GetNetworkId(gameObject);
		if (!networkId.has_value())
		{
			networkId = m_linkingContext.AddGameObject(gameObject);
		}

		stream.Write(networkId.value());

		// Can't to pass value directly to the stream for some reason
		ReplicationClassID temp2 = gameObject->ClassID();
		stream.Write(temp2);
		gameObject->Write(stream);
	}
}

void ReplicationManager::Replicate(InputStream& stream)
{
	std::unordered_set<GameObject*> receivedObjects;

	while (stream.RemainingSize() > 0)
	{
		uint32_t networkId = stream.Read<uint32_t>();
		ReplicationClassID classId = stream.Read<ReplicationClassID>();

		GameObject* gameObject = m_linkingContext.GetGameObject(networkId).value_or(nullptr);
		if (!gameObject)
		{
			gameObject = ClassRegistry::GetInstance().Create(classId);
			m_linkingContext.AddGameObject(gameObject, networkId);
		}

		gameObject->Read(stream);

		receivedObjects.insert(gameObject);
	}

	for (GameObject* gameObject : m_replicatedObjects)
	{
		if (receivedObjects.find(gameObject) == receivedObjects.end())
		{
			m_linkingContext.RemoveGameObject(gameObject);
			gameObject->Destroy();
			delete gameObject;
		}
	}

	m_replicatedObjects = receivedObjects;
}

std::unordered_set<GameObject*> ReplicationManager::GetGameObjects() const
{
	return m_replicatedObjects;
}
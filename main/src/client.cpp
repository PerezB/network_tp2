#include "client.hpp"

#include "streams.hpp"

Client::Client(const std::string_view ip, const unsigned int port)
{
	auto loop = uvw::Loop::getDefault();

	m_clientSocket = loop->resource<uvw::TCPHandle>();
	m_clientSocket->once<uvw::ConnectEvent>(
		[this](const uvw::ConnectEvent&, uvw::TCPHandle& tcp)
		{
			std::cout << "Connection success." << std::endl;
			m_clientSocket->read();
		});

	m_clientSocket->on<uvw::ErrorEvent>(
		[this](const uvw::ErrorEvent& evt, uvw::TCPHandle&) 
		{ 
			OnError(evt);
		});

	m_clientSocket->on<uvw::DataEvent>(
		[this](const uvw::DataEvent& evt, uvw::TCPHandle&) 
		{
			OnData(std::string(evt.data.get(), evt.length));
		});

	m_clientSocket->connect(std::string{ ip.data() }, port);
	m_clientThread = std::make_unique<std::thread>(&Client::Run, this);
}

Client::~Client()
{
	Stop();
}

void Client::Run()
{
	auto loop = uvw::Loop::getDefault();
	loop->run();
}

void Client::Stop()
{
	m_clientSocket->clear();
	m_clientSocket->stop();
	m_clientSocket->close();
	auto loop = uvw::Loop::getDefault();
	loop->stop();
	if (m_clientThread->joinable())
	{
		m_clientThread->join();
	}
}

void Client::OnData(const std::string_view data)
{
	InputStream inputStream(gsl::span<char>(const_cast<char*>(data.data()), data.size()));
	uint32_t protocolId = inputStream.Read<uint32_t>();
	if (protocolId != PROTOCOL_ID)
	{
		std::cout << "Unknow protocol IP in the received packet." << std::endl;
		return;
	}

	uint8_t packetId = inputStream.Read<uint8_t>();
	if (packetId == static_cast<uint8_t>(ReplicationManager::PacketID::Replication))
	{
		m_replicationManager.Replicate(inputStream);
		std::unordered_set<GameObject*> gameObjects = m_replicationManager.GetGameObjects();
		std::for_each(gameObjects.begin(), gameObjects.end(),
			[](GameObject* gameObject)
			{
				std::cout << gameObject->ToString() << std::endl;
				std::cout << " --- " << std::endl;
			});
	}
	else
	{
		std::cout << "Packet type not handled." << std::endl;
	}

}

void Client::OnError(const uvw::ErrorEvent& evt)
{
	std::cout << "Error " << evt.code() << " : " << evt.name() << " - " << evt.what() << std::endl;
}


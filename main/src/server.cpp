#include "server.hpp"

#include <iostream>
#include <algorithm>

Server::Server(const std::string_view ip, const unsigned int port)
{
	auto loop = uvw::Loop::getDefault();

	m_serverSocket = loop->resource<uvw::TCPHandle>();
	m_serverSocket->on<uvw::ListenEvent>([this](const uvw::ListenEvent& event, uvw::TCPHandle& serverSocket)
	{
		OnConnection(serverSocket);
	});

	m_serverSocket->bind(ip.data(), port);
	m_serverSocket->listen();
	m_serverThread = std::make_unique<std::thread>(&Server::Run, this);
}

Server::~Server()
{
	Stop();
}

void Server::Stop()
{
	std::for_each(m_clientSockets.begin(), m_clientSockets.end(),
		[](std::shared_ptr<uvw::TCPHandle> clientSocket)
		{
			clientSocket->clear();
			clientSocket->stop();
			clientSocket->close();
		});
	m_serverSocket->clear();
	m_serverSocket->stop();
	m_serverSocket->close();
	auto loop = uvw::Loop::getDefault();
	loop->stop();

	if (m_serverThread->joinable())
	{
		m_serverThread->join();
	}
}

void Server::OnConnection(uvw::TCPHandle& serverSocket)
{
	std::shared_ptr<uvw::TCPHandle> clientSocket = serverSocket.loop().resource<uvw::TCPHandle>();

	clientSocket->on<uvw::EndEvent>(
		[this](const uvw::EndEvent&, uvw::TCPHandle& client) 
		{ 
			OnCloseEvent(client);
		});

	serverSocket.accept(*clientSocket);
	uvw::Addr clientInfos = clientSocket->peer();
	m_clientSockets.push_back(clientSocket);
	std::cout << "Client " << clientInfos.ip << " connected from port " << clientInfos.port << "." << std::endl;

	clientSocket->read();
}

void Server::Run()
{
	auto loop = uvw::Loop::getDefault();
	loop->run();
}

void Server::OnCloseEvent(uvw::TCPHandle& clientSocket)
{
	auto clientIterator = std::find(m_clientSockets.begin(), m_clientSockets.end(), clientSocket.shared_from_this());
	if (clientIterator == m_clientSockets.end())
	{
		std::cout << "Error: receiving close event from an unknown client." << std::endl;
	}
	else
	{
		uvw::Addr clientInfos = clientSocket.peer();
		std::cout << "Client " << clientInfos.ip << ":" << clientInfos.port << " disconnected." << std::endl;
		m_clientSockets.erase(clientIterator);
		clientSocket.close();
	}
}

void Server::Send(uint8_t * packet, size_t packetSize)
{
	std::for_each(m_clientSockets.begin(), m_clientSockets.end(),
		[packet, packetSize](std::shared_ptr<uvw::TCPHandle> clientSocket)
		{
			if (clientSocket->writable())
			{
				clientSocket->write(reinterpret_cast<char * >(packet), packetSize);
			}
		});
}


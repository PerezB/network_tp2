#pragma once

#include <unordered_map>
#include <functional>
#include <cassert>

#include "game_object.hpp"

class ClassRegistry
{
public:
	ClassRegistry(const ClassRegistry&) = delete;
	ClassRegistry& operator=(const ClassRegistry&) = delete;
	ClassRegistry(ClassRegistry&&) = delete;
	ClassRegistry& operator=(ClassRegistry&&) = delete;

	static ClassRegistry& GetInstance() {
		static ClassRegistry s_instance;
		return s_instance;
	}

	GameObject* Create(ReplicationClassID id) const;

	template<typename T>
	void RegisterClass()
	{
		assert(m_classRegistry.find(T::mClassID) == m_classRegistry.end());

		m_classRegistry.insert(std::make_pair(T::mClassID, &T::CreateInstance));
	}

private:
	ClassRegistry();

	std::unordered_map<ReplicationClassID, std::function<GameObject*()>> m_classRegistry;
};
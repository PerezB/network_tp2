#pragma once

#include <iostream>
#include <memory>
#include <string>
#include <uvw.hpp>
#include <thread>
#include <unordered_set>

#include "replication_manager.hpp"

class Client
{
public:
	Client(const std::string_view ip, const unsigned int port);
	~Client();

	void Stop();

private:
	std::shared_ptr<uvw::TCPHandle> m_clientSocket;
	std::unique_ptr<std::thread> m_clientThread;

	ReplicationManager m_replicationManager;

	void Run();
	void OnData(const std::string_view data);
	void OnError(const uvw::ErrorEvent& evt);
};


#pragma once

#include "game_object.hpp"
#include <string>

class Enemy : public GameObject
{
public:
	REPLICATED('ENEM', Enemy)
	Enemy();
	void Write(OutputStream& stream) override;
	void Read(InputStream& stream) override;
	void Destroy() override;

	void SetPos(float x, float y, float z);
	std::string ToString() const override;

private:
	float m_xPos = 0;
	float m_yPos = 0;
	float m_zPos = 0;

	std::string m_type = "EnnemyType";

	float m_xRot = 1;
	float m_yRot = 0;
	float m_zRot = 0;
	float m_tRot = 0;

	bool m_firstRead = true; // just for display when we update an existing object
};
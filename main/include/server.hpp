#pragma once

#include <memory>
#include <uvw.hpp>
#include <vector>
#include <thread>

class Server
{
public:
	Server(const std::string_view ip, const unsigned int port);
	~Server();

	void Send(uint8_t * const packet, size_t packetSize);
	void Stop();

private:
	std::vector<std::shared_ptr<uvw::TCPHandle>> m_clientSockets;
	std::shared_ptr<uvw::TCPHandle> m_serverSocket;
	std::unique_ptr<std::thread> m_serverThread;

	void Run();
	void OnConnection(uvw::TCPHandle& serverSocket);
	void OnCloseEvent(uvw::TCPHandle& clientSocket);
};
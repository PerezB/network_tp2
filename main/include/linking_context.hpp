#pragma once 

#include <unordered_map>
#include <optional>

#include <game_object.hpp>

class LinkingContext
{
public:
	LinkingContext();

	uint32_t AddGameObject(GameObject* object);
	void AddGameObject(GameObject* object, uint32_t networkId);
	void RemoveGameObject(GameObject* object);

	std::optional<uint32_t> GetNetworkId(GameObject* object);
	std::optional<GameObject*> GetGameObject(uint32_t networkId);

private:
	std::unordered_map<uint32_t, GameObject*> m_idToGameObject;
	std::unordered_map<GameObject*, uint32_t> m_gameObjectToId;
	uint32_t m_nextId = 0;
};
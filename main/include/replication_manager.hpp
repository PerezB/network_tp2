#pragma once

#include <cstdint>
#include <unordered_set>

#include "game_object.hpp"
#include "streams.hpp"
#include "linking_context.hpp"

//Compilation on Linux does not if PROTOCOL_ID is defined inside the ReplicationManager class
static const uint32_t PROTOCOL_ID = 0xC0FFEE;

class ReplicationManager
{
public:

	enum class PacketID : uint8_t
	{
		Hello = 0x00,
		Replication = 0x01,
		Bye = 0x02,
		Max
	};

	void Replicate(OutputStream& stream, const std::vector<GameObject*>& gameObjects);
	void Replicate(InputStream& stream);
	std::unordered_set<GameObject*> GetGameObjects() const;

private:
	std::unordered_set<GameObject*> m_replicatedObjects;

	LinkingContext m_linkingContext;
};
# Simple Replication System

Project made in 2019 for the Networking class at UQAC. <br/>
It is a simple replication system for games. It replicates the whole world state. <br/>
We used uvw as network library. It is available here: https://github.com/skypjack/uvw.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

The things you need to install the software and how to install them

- A compiler, for obvious reasons,
- CMake, it will be used to generate the files for your environment.

Note that the project has been made with Visual Studio so using it could help you running the project. If you use Visual Studio Code, don't forget to add the CMake and CMake Tools extensions.

### Installing

A step by step series of examples that tell you how to get a development environment running

1. Clone the project on your computer

#### If you have Visual Studio or Visual Studio Code

2. Right click on the folder with the project name --> Open in Visual Studio
3. Once you're inside Visual Studio, generate everything. On VS19 Build --> BuildAll. You should see CMake working in the console output. When it's done you should see "Build All succeed" written in the consol output.
4. Run the program. On VS19 Debug --> Start. You should see the OpenGLWindow. If Start button is disabled, check if the correct item is selected in the "Select Startup Item" drop-down list (on the top of the screen, on the right of the green arrow). The selected item should be MyExe.exe.

The process is similar on Visual Studio Code.

#### If you're not on Visual Studio

2. Use CMake to generate the files for your environment.

The next steps depend on your environment but you should be able to:

3. Compile the CMake generated files.
4. Run the program.


## Start the project

### Server side 
To start the server: <br/>
first parameter: "server" <br/>
second parameter: ip <br/>
third parameter: port

### Client side
To start the client: <br/>
first parameter: "client" <br/>
second parameter: ip <br/>
third parameter: port

## Authors

* **BERTRAND Axel**
* **DURAND Quentin**
* **PEREZ Benjamin**

## My contribution

In this project, I participated a bit to everything but my main job was to assemble every parts together and make them work.


